//
//  ContentView.swift
//  CoronaTracker
//
//  Created by Alan Santoso on 16/10/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
