//
//  CoronaTrackerApp.swift
//  CoronaTracker
//
//  Created by Alan Santoso on 16/10/20.
//

import SwiftUI

@main
struct CoronaTrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
